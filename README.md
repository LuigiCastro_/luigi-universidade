# Observações:
        01. Façam a deleção das tabelas existentes neste banco, caso existam, para evitar o erro de duplicidade de tabelas ao executarem as migrations;
        02. A população de dados deverá ser feito via API, igualmente feito na aula;
        03. Para ter um exercício que atenda todos os critérios de avaliação, é indicado ter como base o projeto de exemplo, criado na aula da semana 9.

# Vocês deverão realizar a seguinte implementação:
    01. Todas entidades ter os campos: "Id", "UsuarioCriacao", "DataCriacao", "UsuarioAlteracao", "DataAlteracao", "Ativo";
    02. Ter a entidade "Usuario", que não tem na modelagem relacional em anexo;
    03. A senha deve ser criptografada;
    04. Api deverá ter autenticação JWT;
    05. Criar novas migrations para as tabelas necessárias (exceto a tabela PreRequisito);
    06. Criar o CRUD completo de todas os "Controllers" ;
    07. É necessário que tenha o swagger, para a entrega do exercício;
    08. É necessário ter um PATH em cada "Controller" para alterar pelo menos uma coluna;
    09. É necessário ter uma busca de algum campo específico em cada 
    "Controller";
    10. No "Controller" endereço, ter um endpoint de busca por CEP, onde irá ter a integração com (https://brasilapi.com.br/docs#tag/CEP).
    11. Ter dois perfis "Aluno" e "Professor", o perfil "Aluno" só poderá manipular o seu registro da entidade "aluno" ou "usuario" e o perfil "Professor" poderá ter o total acesso.
    12. Deletar logicamente com o campo "Ativo";
    13. Todos os métodos devem ser async/await;
    14. Deve ser usado a injeção de dependência.

# Entrega:
    01. Criar o repositório pessoal deste projeto no gitlab, com o nome "{nomealuno}-universidade";
    02. Adicionar o usuário @cezarpedross como membro desse código;
    03. Ao final vocês deverão anexar no classroom o link do repositório utilizado.



___________________________________________________________________________________________



# Nova estrutura DDD:
> API
    - Controller
    - Program
    - Profiles
        - mapping profile
> DOMAIN
    - Entities
    - Contracts
        - request
        - response
    - Enums
    - Interfaces
    - Services
    - Settings                  [string SqlServerConnection]
> INFRASTRUCTURE
    - Repositorories            [*BaseRepository, *BaseApiRepository]
    - Context
    - Migrations



**Utilzizar maps apenas na controller, na service não.
**Injeção de dependencia, mappers, dbcontext, settings, swagger (todos em execução no Program).
**Base p/ tudo : iservice, irepository, service, repository, entities, controller, response?
**DTO é o mesmo que Contracts?
**Criar a entidade Perfil. Inclusive migrar a tabela.
**Link connectionString no appsetings.json


SLN -------- Universidade.DDD
    API -------- Universidade.Api
    DOMAIN ----- Universidade.Domain
    INFRA ------ Universidade.Infra