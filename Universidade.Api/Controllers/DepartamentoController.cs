using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Entities;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Universidade.Domain.Utils;

namespace Universidade.Api.Controller;

[Authorize(Roles = ConstanteUtil.PerfilProfessorNome)]
public class DepartamentoController : BaseController<DepartamentoEntity, DepartamentoRequest, DepartamentoResponse>
{
    private readonly IMapper _mapper;
    private readonly IDepartamentoService _departamentoService;
    public DepartamentoController(IMapper mapper, IDepartamentoService departamentoService) : base(mapper, departamentoService)
    {
        _mapper = mapper;
        _departamentoService = departamentoService;
    }


    [HttpPatch("{id}")]
    [ProducesResponseType(200)]
    public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] DepartamentoNomeRequest departamentoRequest)
    {
        await _departamentoService.AtualizarNomeAsync(id, departamentoRequest.Nome);
        return Ok();
    }


    [HttpGet("[controller]{codigo}")]
    [ProducesResponseType(200)]
    public virtual async Task<ActionResult<DepartamentoResponse>> GetByCodigoAsync([FromRoute] string codigo)
    {
        var entity = await _departamentoService.ObterDepartamentoPorCodigoAsync(codigo);
        var reponse = _mapper.Map<DepartamentoResponse>(entity);
        return Ok(reponse);
    }
}