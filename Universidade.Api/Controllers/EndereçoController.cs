using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Entities;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Universidade.Domain.Utils;

namespace Universidade.Api.Controller;

[Authorize(Roles = ConstanteUtil.PerfilProfessorNome)]
public class EndereçoController : BaseController<EndereçoEntity, EndereçoRequest, EndereçoResponse>
{
    private readonly IMapper _mapper;
    private readonly IEndereçoService _endereçoService;
    public EndereçoController(IMapper mapper, IEndereçoService endereçoService) : base(mapper, endereçoService)
    {
        _mapper = mapper;
        _endereçoService = endereçoService;
    }


    [HttpPatch("{id}")]
    [ProducesResponseType(200)]
    public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] EndereçoComplementoRequest endereçoRequest)
    {
        await _endereçoService.AtualizarComplementoAsync(id, endereçoRequest.Complemento);
        return Ok();
    }


    [HttpGet("[controller]{cep}")]
    [ProducesResponseType(200)]
    public virtual async Task<ActionResult<EndereçoResponse>> GetByCepAsync([FromRoute] string cep)
    {
        var entity = await _endereçoService.ObterEndereçoPorCepAsync(cep);
        var reponse = _mapper.Map<EndereçoResponse>(entity);
        return Ok(reponse);
    }
}