using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Universidade.Domain.Entities;
using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Contracts.Response;
using Universidade.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Universidade.Domain.Utils;

namespace Universidade.Api.Controller;

[Authorize(Roles = ConstanteUtil.PerfilProfessorNome)]
public class CursoController : BaseController<CursoEntity, CursoRequest, CursoResponse>
{
    private readonly IMapper _mapper;
    private readonly ICursoService _cursoService;
    public CursoController(IMapper mapper, ICursoService cursoService) : base(mapper, cursoService)
    {
        _mapper = mapper;
        _cursoService = cursoService;
    }


    [HttpPatch("{id}")]
    [ProducesResponseType(200)]
    public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] CursoTurnoRequest cursoRequest)
    {
        await _cursoService.AtualizarTurnoAsync(id, cursoRequest.Turno);
        return Ok();
    }


    [HttpGet("[controller]{codigo}")]
    [ProducesResponseType(200)]
    public virtual async Task<ActionResult<CursoResponse>> GetByCodigoAsync([FromRoute] string codigo)
    {
        var entity = await _cursoService.ObterCursoPorCodigoAsync(codigo);
        var reponse = _mapper.Map<CursoResponse>(entity);
        return Ok(reponse);
    }
}