using Universidade.Domain.Contracts.Response;

namespace Universidade.Domain.Contracts.Requests;

public class PerfilResponse : BaseResponse
{
    public string PerfilUsuario { get; set; }
}