using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Entities;

namespace Universidade.Domain.Contracts.Response;

public class DepartamentoResponse : BaseResponse
{
    public string Nome {get; set;}
    public string Codigo {get; set;}

    public int EndereçoId {get; set;}
}