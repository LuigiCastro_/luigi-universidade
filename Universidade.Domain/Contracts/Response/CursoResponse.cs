using Universidade.Domain.Contracts.Requests;
using Universidade.Domain.Entities;


namespace Universidade.Domain.Contracts.Response;

public class CursoResponse : BaseResponse
{
    public string Nome {get; set;}
    public string Turno {get; set;}
    public string Duraçao {get; set;}
    public string TipoCurso {get; set;}
    public string Codigo {get; set;}

    public int DepartamentoId {get; set;}

}