namespace Universidade.Domain.Contracts.Requests;


public class DepartamentoNomeRequest
{
    public string Nome { get; set; }
}
