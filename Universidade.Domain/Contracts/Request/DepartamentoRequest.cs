namespace Universidade.Domain.Contracts.Requests;

public class DepartamentoRequest
{
    public string Nome {get; set;}
    public string Codigo {get; set;}
    public int EndereçoId {get; set;}
    
}