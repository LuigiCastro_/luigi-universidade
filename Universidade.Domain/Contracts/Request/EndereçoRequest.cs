namespace Universidade.Domain.Contracts.Requests;

public class EndereçoRequest
{
    public string Rua {get; set;}
    public string Bairro {get; set;}
    public string Complemento {get; set;}
    public string Cidade {get; set;}
    public string Estado {get; set;}
    public string Cep {get; set;}
}