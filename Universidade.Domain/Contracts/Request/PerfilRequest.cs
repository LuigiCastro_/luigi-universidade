namespace Universidade.Domain.Contracts.Requests;

public class PerfilRequest
{
    public string PerfilUsuario { get; set; }
}