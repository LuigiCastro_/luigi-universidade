// using System.Linq.Expressions;
// using Universidade.Domain.Entities;
// using Universidade.Domain.Interfaces.Repositories;
// using Universidade.Domain.Contracts.Response;
// using Universidade.Domain.Interfaces.Services;
// using System.IdentityModel.Tokens.Jwt;
// using Microsoft.IdentityModel.Tokens;
// using Universidade.Domain.Settings;
// using System.Security.Claims;
// using System.Text;
// using Universidade.Domain.Service;
// using Microsoft.AspNetCore.Http;
// using Academy.DDD.Domain.Exceptions;

// namespace Universidade.Domain.Service;

// public class AutenticaçaoService : IAutenticaçaoService
// {
//     private readonly IUsuarioRepository _repository;
//     private readonly AppSettings _appSettings;
//     private readonly BaseService<UsuarioEntity> _baseService;


//     public AutenticaçaoService(IUsuarioRepository usuarioRepository, AppSettings appSettings)
//     {
//         _repository = usuarioRepository;
//         _appSettings = appSettings;
//     }
//     public async Task<AutenticaçaoResponse> AutenticarAsync(string email, string senha)
//     {
//         var entity = await _baseService.ObterAsync(x => x.Email.Equals(email) && x.Ativo);

//         if (!BCrypt.Net.BCrypt.Verify(senha, entity.Senha))
//                 throw new InformacaoException(Enums.StatusException.FormatoIncorreto, "Usuário ou senha incorreta");

//         var tokenDescriptor = new SecurityTokenDescriptor
//         {
//             Subject = new ClaimsIdentity(new Claim[]
//             {
//                 new Claim(ClaimTypes.NameIdentifier, entity.Id.ToString()),
//                 new Claim(ClaimTypes.Name, entity.Nome),
//                 new Claim(ClaimTypes.Email, entity.Email),
//                 new Claim(ClaimTypes.Role, entity.PerfilUsuario.PerfilUsuario)
//             }),
//             Expires = DateTime.UtcNow.AddDays(1),
//             SigningCredentials = new SigningCredentials(
//                 new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.JwtSecurityKey)),
//                 SecurityAlgorithms.HmacSha256Signature)
//         };

//         var tokenHandler = new JwtSecurityTokenHandler();
//         var token = tokenHandler.CreateToken(tokenDescriptor);
//         var tokenString = tokenHandler.WriteToken(token);

//         return new AutenticaçaoResponse
//         {
//             Token = tokenString,
//             DataExpiracao = tokenDescriptor.Expires
//         };
//     }
// }