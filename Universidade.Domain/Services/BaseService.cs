using System.Linq.Expressions;
using System.Security.Claims;
using Universidade.Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;
using Universidade.Domain.Interfaces.Services;
using Universidade.Domain.Utils;

namespace Universidade.Domain.Service;

public class BaseService<T> : IBaseService<T> where T : BaseEntity
{
    private readonly IBaseRepository<T> _repository;
    public readonly int? UserId;
    public readonly string UserPerfil;
    public BaseService(IBaseRepository<T> repository, IHttpContextAccessor httpContextAccessor)
    {
        _repository = repository;
        UserId = httpContextAccessor.HttpContext.GetClaim(ClaimTypes.NameIdentifier).ToInt();
        UserPerfil = httpContextAccessor.HttpContext.GetClaim(ClaimTypes.Role);
    }

    public async Task<List<T>> ObterTodosAsync(Expression<Func<T, bool>> expression)
    {
        return await _repository.ListAsync(expression);
    }

    public async Task<List<T>> ObterTodosAsync()
    {
        return await _repository.ListAsync(x => x.Ativo);
    }
    public async Task<T> ObterAsync(Expression<Func<T, bool>> expression)
    {
        var entity = await _repository.FindAsync(expression);
        if (entity == null)
            throw new InformacaoException(Enums.StatusException.NaoEncontrado, $"Nenhum dado encontrado");

        return entity;
    }

    public async Task<T> ObterPorIdAsync(int id)
    {
        var entity = await _repository.FindAsync(x => x.Id == id && x.Ativo);
        return entity;
    }
    public async Task AdicionarAsync(T entity)
    {
        entity.DataCriacao = DateTime.Now;
        await _repository.AddAsync(entity);
    }
    public async Task AlterarAsync(T entity)
    {
        var find = await _repository.FindAsNoTrackingAsync(x => x.Id == entity.Id && x.Ativo);
        entity.DataCriacao = find.DataCriacao;
        entity.DataAlteracao = DateTime.Now;
        await _repository.EditAsync(entity);
    }
    public async Task DeletarAsync(int id)
    {
        var entity = await _repository.FindAsync(id);
        entity.DataAlteracao = DateTime.Now;
        entity.Ativo = false;
        await _repository.EditAsync(entity);
    }
}