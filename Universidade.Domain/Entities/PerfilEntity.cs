namespace Universidade.Domain.Entities;

public class PerfilEntity : BaseEntity
{
    public string PerfilUsuario { get; set; }

    public virtual ICollection<UsuarioEntity> Usuarios { get; set; }
}