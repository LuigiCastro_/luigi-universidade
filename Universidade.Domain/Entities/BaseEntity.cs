namespace Universidade.Domain.Entities;

public class BaseEntity
{
    public BaseEntity()
    {
        Ativo = true;
    }
    public int Id {get; set;}
    public int? UsuarioCriacao {get; set;}
    public DateTime DataCriacao {get; set;}
    public int? UsuarioAlteracao {get; set;}
    public DateTime DataAlteracao {get; set;}
    public bool Ativo {get; set;}
}