using Universidade.Domain.Entities;

namespace Universidade.Domain.Interfaces.Repositories;

public interface ICursoRepository : IBaseRepository<CursoEntity>
{

}