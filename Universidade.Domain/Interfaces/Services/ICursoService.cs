using Universidade.Domain.Entities;

namespace Universidade.Domain.Interfaces.Services;

public interface ICursoService : IBaseService<CursoEntity>
{
    Task AtualizarTurnoAsync(int id, string turno);
    Task<CursoEntity> ObterCursoPorCodigoAsync(string codigo);
}