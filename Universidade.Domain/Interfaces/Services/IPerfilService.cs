using Universidade.Domain.Entities;

namespace Universidade.Domain.Interfaces.Services;

public interface IPerfilService : IBaseService<PerfilEntity>
{
    Task AtualizarPerfilUsuarioAsync(int id, string perfilUsuario);
}