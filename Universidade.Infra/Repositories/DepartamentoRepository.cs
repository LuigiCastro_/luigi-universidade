using Universidade.Infra.Context;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;

namespace Universidade.Infra.Repositories;

public class DepartamentoRepository : BaseRepository<DepartamentoEntity>, IDepartamentoRepository
{
    public DepartamentoRepository(ManutençaoContext context) : base(context)
    {
        
    }
    
}