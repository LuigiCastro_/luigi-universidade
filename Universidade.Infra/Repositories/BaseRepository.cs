using System.Linq.Expressions;
using Universidade.Infra.Context;
using Universidade.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Universidade.Infra.Repositories;

public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
{
    private readonly ManutençaoContext _context;
    public BaseRepository(ManutençaoContext context)
    {
        _context = context;
    }

    public async Task<T> FindAsync(int id)
    {
        return await _context.Set<T>().FindAsync(id);
    }
    // public async Task<T> FindAsync(decimal id)
    // {
    //     throw new NotImplementedException();
    // }
    public async Task<T> FindAsync(Expression<Func<T, bool>> expression)
    {
        return await _context.Set<T>().FirstOrDefaultAsync(expression);
    }
    public async Task<T> FindAsNoTrackingAsync(Expression<Func<T, bool>> expression)
    {
        return await _context.Set<T>().AsNoTracking().FirstOrDefaultAsync(expression);
    }
    // public async Task<int> CountAsync()
    // {
    //     throw new NotImplementedException();
    // }
    // public async Task<int> CountAsync(Expression<Func<T, bool>> expression)
    // {
    //     throw new NotImplementedException();
    // }
    // public async Task<int> CountAsync<K>(Expression<Func<T, IEnumerable<K>>> selectExpression)
    // {
    //     throw new NotImplementedException();
    // }
    // public async Task<int> CountAsync<K>(Expression<Func<T, bool>> expression, Expression<Func<T, IEnumerable<K>>> selectExpression)
    // {
    //     throw new NotImplementedException();
    // }
    public async Task<List<T>> ListAsync()
    {
        return await _context.Set<T>().ToListAsync();
    }
    public async Task<List<T>> ListAsync(Expression<Func<T, bool>> expression)
    {
        return await _context.Set<T>().Where(expression).ToListAsync();
    }
    // public async Task<List<T>> ListPaginationAsync<K>(Expression<Func<T, K>> sortExpression, int pagina, int quantidade)
    // {
    //     throw new NotImplementedException();
    // }
    // public async Task<List<T>> ListPaginationAsync<K>(Expression<Func<T, bool>> expression, Expression<Func<T, K>> sortExpression, int pagina, int quantidade)
    // {
    //     throw new NotImplementedException();
    // }
    public async Task AddAsync(T item)
    {
        await _context.Set<T>().AddAsync(item);
        await _context.SaveChangesAsync();
    }
    public async Task EditAsync(T item)
    {
        _context.Set<T>().Update(item);
        await _context.SaveChangesAsync();
    }
    public async Task RemoveAsync(T item)
    {
        _context.Set<T>().Remove(item);
        await _context.SaveChangesAsync();
    }

}