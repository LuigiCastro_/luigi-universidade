using Universidade.Infra.Context;
using Universidade.Domain.Entities;
using Universidade.Domain.Interfaces.Repositories;

namespace Universidade.Infra.Repositories;

public class EndereçoRepository : BaseRepository<EndereçoEntity>, IEndereçoRepository
{
    public EndereçoRepository(ManutençaoContext context) : base(context)
    {
        
    }
    
}